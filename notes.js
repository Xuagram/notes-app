const fs = require('fs')
const chalk = require('chalk')

const addNote = (title, body) => {
    const notes = loadNotes()
    const duplicateNote = notes.find((note) => note.title === title)

    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse('New note added!'))
    } else {
        console.log(chalk.red.inverse('Note title taken!'))
    }
}

const removeNote = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.title !== title)
    saveNotes(notesToKeep)
    console.log(title)
    if (notes.length !== notesToKeep.length)
        console.log(chalk.black.bgGreen('Note removed!'))
    else 
        console.log(chalk.black.bgRed('No note found!'))
}


const readNote = (title) => {
    const notes = loadNotes()
    
    const noteToRead = notes.find((note) => note.title === title)
    console.log(title)
    if (noteToRead)
        console.log(chalk.black.bgGreen(noteToRead.body))
    else 
        console.log(chalk.black.bgRed('No note found!'))
}

const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return (JSON.parse(dataJSON)) 
    }   catch (e) {
        return []
    }
}

const listNote = () => { 
    console.log(chalk.blue('This is your notes:'))
    const notes = loadNotes()
    notes.forEach(note => console.log(chalk.inverse(note.title)))
}


module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNote: listNote,
    readNote: readNote
}