# Notes App

A note-taking application in Javascript that can be used to add, remove, list or read notes. It uses yargs and chalk modules.

# Credits

This application has been developed according to an online-class on Udemy (https://www.udemy.com/course/the-complete-nodejs-developer-course-2/) by Andrew Mead, in order to learn how to code in nodejs. It is a very beginner project as it is the first project of the online-course. I am actually at the middle of the course, learning how to build a web server with express module in node.js

# How to run

This program shell can be compiled on all unix system.

- Clone this repository : `git clone https://gitlab.com/Xuagram/notes-app.git`

- Change directory into the new directory

`cd notes-app`

- Install the node modules packages

`npm install`

- To see the usage run the following command

`node app.js help`

- To add a note run the following command replacing with your own data

`node app.js add --title="Shopping list" --body="a lot of stuff"`

- To list all the notes run the following command

`node app.js list`

- To read a note run the following command replacing with your own data

`node app.js read --title="Shopping list"`

- To delete a note run the following command replacing with your own data

`node app.js remove --title="Shopping list"`

Enjoy !